﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527ASS1
{
    [Serializable]
    public class Videogame
    {
        private String title;
        private String genre;
        private String publisher;
        private String developer;
        private String img;
        private DateTime releaseDate = new DateTime();

        public Videogame()
        {

        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        public String Genre
        {
            get { return genre; }
            set { genre = value; }
        }
        public String Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }
        public String Developer
        {
            get { return developer; }
            set { developer = value; }
        }
        public String Img
        {
            get { return img; }
            set { img = value; }
        }

        public DateTime ReleaseDate
        {
            get { return releaseDate; }
            set { releaseDate = value; }
        }

        public override string ToString()
        {
            String output = "Title: " + title +
                '\n' + "Genre: " + genre +
                '\n' + "Publisher: " + publisher +
                '\n' + "Developer: " + developer +
                '\n' + "Release date: " + releaseDate.ToShortDateString();
            return output;
        }
    }
}
