﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ICTPRG527ASS1
{
    class FileManager
    {
        static String fileName = "data.bin";
        static String filePath = System.IO.Directory.GetCurrentDirectory();
        static String fileAndPath = System.IO.Path.Combine(filePath, fileName);

        static public void SaveData()
        {
            //serialize the data
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fileAndPath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, Globals.library);
            stream.Close();
        }

        static public void LoadData()
        {
            //create a formatter object
            IFormatter formatter = new BinaryFormatter();

            //check to see if the file exists
            try
            {
                if (File.Exists(fileAndPath))
                {
                    using (Stream stream = new FileStream(fileAndPath, 
                        FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        //save to the library object
                        Globals.library = (Library)formatter.Deserialize(stream);
                    }
                }
                else
                {
                    //file does not exist, show a messagebox
                    String msg = "Unable to find the file.\nCreating an empty library";
                    MessageBox.Show(msg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    //create a file
                    using (FileStream fs = File.Create(fileAndPath))
                    {
                        //nothing to do here. stream will close by itself
                    }
                }
            }
            catch(Exception)
            {
                //An error occurred
                String msg = "Error encountered while attempting to load file";
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static private Bitmap LoadUnlockedImage(String fileName)
        {
            using (Bitmap bm = new Bitmap(fileName))
            {
                //return bitmap with the copy constructor
                return new Bitmap(bm);
            }
        }

        static public void LoadImage(String fileName, PictureBox pBox)
        {
            //dispose of the previous image, if required
            if (pBox.Image != null)
            {
                pBox.Image.Dispose();
            }
            //load the unlocked image
            pBox.Image = LoadUnlockedImage(fileName);
        }
    }
}
