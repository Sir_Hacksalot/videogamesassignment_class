﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527ASS1
{
    public partial class LibraryForm : Form
    {
        static int currentIndex = 0;

        public LibraryForm()
        {
            InitializeComponent();
        }

        private void addToLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //create an AddItemForm object, then open it
            AddItemForm addForm = new AddItemForm();
            addForm.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }

        private void gettingStartedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"help\index.html");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileManager.SaveData();
            this.Close();
        }

        private void LibraryForm_Load(object sender, EventArgs e)
        {
            //load the data
            FileManager.LoadData();
            //fill combobox
            FillComboBox();
            //load item 0
            ShowInfo(0);
        }

        private void FillComboBox()
        {
            //fill the combobox from the library object
            foreach(var item in Globals.library.VideoLibrary)
            {
                comboBox1.Items.Add(item.Title);
            }
        }



        private void ShowInfo(int index)
        {
            currentIndex = index;

            //check if there are any items in the library
            if(Globals.library.VideoLibrary.Count > 0)
            {
                //load the info into the textbox
                textBox1.Text = Globals.library.VideoLibrary[index].ToString();

                //load the image if it exists
                String path = @"artwork\" + Globals.library.VideoLibrary[index].Img + ".png";
                if (File.Exists(path))
                {
                    //load the file
                    FileManager.LoadImage(path, pictureBox1);
                }
                else
                {
                    //show a default image from resources
                    pictureBox1.Image = Properties.Resources.noimage;
                }
            }
            else
            {
                //nothing in the file to display
                pictureBox1.Image = Properties.Resources.noimage;
                //clear the textbox
                textBox1.Clear();
                //clear the combobox text
                comboBox1.Text = "";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get the selected index
            int index = comboBox1.SelectedIndex;

            //show the info
            ShowInfo(index);
        }

        private void LibraryForm_Activated(object sender, EventArgs e)
        {
            if (Globals.LibraryChanged)
            {
                //library has been changed, refill combobox

                //empty the combobox
                comboBox1.Items.Clear();
                //fill the combobox
                FillComboBox();
                //reset the boolean
                Globals.LibraryChanged = false; 
            }
            else
            {
                //do nothing
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (Globals.library.VideoLibrary.Count > 0)
            {
                //delete file
                File.Delete(@"artwork\" + Globals.library.VideoLibrary[currentIndex].Img + ".png");
                //remove the item from the library
                Globals.library.VideoLibrary.RemoveAt(currentIndex);
                //remove item from combobox
                comboBox1.Items.RemoveAt(currentIndex);

                //set the previous image and info
                ShowPrevious();
            }
            else
            {
                //do nothing
            }
        }

        private void ShowPrevious()
        {
            if (Globals.library.VideoLibrary.Count > 0)
            {
                if(currentIndex > 0)
                {
                    ShowInfo(currentIndex - 1);
                }
                else
                {
                    ShowInfo(Globals.library.VideoLibrary.Count - 1);
                }
            }
            else
            {
                //Display no info
                ShowInfo(0);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileManager.SaveData();
        }

        private void LibraryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            FileManager.SaveData();
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            ShowPrevious();
        }
    }
}
