﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527ASS1
{
    public partial class AddItemForm : Form
    {
        public AddItemForm()
        {
            InitializeComponent();
            ((Control)pictureBox1).AllowDrop = true;
        }

        private Boolean InputValidation()
        {
            if (titleTextBox.Text != "")
            {
                if (genreTextBox.Text != "")
                {
                    if (publisherTextBox.Text != "")
                    {
                        if (developerTextBox.Text != "")
                        {
                            return true;
                        }
                        else
                        {
                            developerTextBox.Focus();
                        }
                    }
                    else
                    {
                        publisherTextBox.Focus();
                    }
                }
                else
                {
                    genreTextBox.Focus();
                }
            }
            else
            {
                //send focus back to the textbox
                titleTextBox.Focus();
            }
            return false;
        }

        private Videogame GetText()
        {
            //create object
            Videogame vgame = new Videogame();
            vgame.Title = titleTextBox.Text;
            vgame.Genre = genreTextBox.Text;
            vgame.Publisher = publisherTextBox.Text;
            vgame.Developer = developerTextBox.Text;
            vgame.ReleaseDate = releaseDateTimePicker.Value;
            return vgame;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (InputValidation())
            {
                //create object
                Videogame vgame = GetText();
                SaveImage(sender, e, vgame);
                if(pictureBox1.Image != null)
                {
                    //save the data
                    Globals.library.VideoLibrary.Add(vgame);
                    FileManager.SaveData();
                    //let the Form 1 know we have updated the library via Globals
                    Globals.LibraryChanged = true;
                    
                    //flash save message
                    this.Text += "\t\t\t\t\t-File Saved.";
                    timer1.Start();
                }
                else
                {
                    //missing an image file, no saving.
                    MessageBox.Show("Missing an image file.", "", MessageBoxButtons.OK, 
                        MessageBoxIcon.Information);
                }
                
            }
              
      
        }

        private void SaveImage(object sender, EventArgs e, Videogame vgame)
        {
            if (pictureBox1.Image != null)
            {
                try
                {
                    pictureBox1.Image.Tag = vgame.Title;
                    vgame.Img = pictureBox1.Image.Tag.ToString();
                    //save the image somwhere
                    pictureBox1.Image.Save(@"artwork\" + vgame.Img + ".png", ImageFormat.Png);
                    
                }
                catch (Exception)
                {
                    //bad things happened
                    MessageBox.Show("An error occured while attempting to save the file.");
                   pictureBox1_Click(sender, e);
                }
            }
            else
            {
                //open the filepicker
                pictureBox1_Click(sender, e);
            }
        }

        private void pictureBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
            String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
            //get and assign the file extension to a string
            String ext = Path.GetExtension(files[0].ToLower());

            //find out what we have
            if (ext == ".jpeg" || ext == ".jpg" || ext == ".bmp" || ext == ".png")
            {
                
                try
                {
                    //assign the image
                    pictureBox1.Image = Image.FromFile(files[0]);
                }
                catch(OutOfMemoryException)
                {
                    MessageBox.Show("Cannot display file.", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }        
            }
            else
            {
                //nothing to do here
                MessageBox.Show("Incorrect file type.", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }       
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //open a dialog where we can pick a file
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Choose an image";
                ofd.Filter = "Image Files (*.bmp;*.jpg;*.jpeg; *.png;) | *.BMP;*.JPG;*.JPEG;*.PNG";
                ofd.InitialDirectory = "Pictures";

                //open the file picker dialog
                if(ofd.ShowDialog()  == DialogResult.OK)
                {
                    //if the user hits ok, assign the image
                    pictureBox1.Image = new Bitmap(ofd.FileName);
                    
                }

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //flash text up on the titlebar
            this.Text = "addItemForm";
            timer1.Stop();
        }
    }
}
